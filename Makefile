
image:
	docker build -t kuriyama/node:12 -f Dockerfile.12 .
	docker build -t kuriyama/node:14 -f Dockerfile.14 .

pull:
	@docker pull node:12 > .log-12
	@docker pull node:14 > .log-14
	@docker image inspect node:12 | jq -r '.[0].Id' > .id-12
	@docker image inspect node:14 | jq -r '.[0].Id' > .id-14

check:
	@if git status -s | grep -q M; then\
		git add .etag .id-12 .id-14 && git commit -m "Update IDs."; git push;\
	fi

etag:
	@curl -s -D- --head HEAD https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip | \
	grep ^ETag | uniq | sed -e 's|^ETag: ||' | jq -r . > .etag
#	@cat .etag
